function confirmarLimparTela() {
    $("#confirmarLimparTelar").modal();

}

function limparTela() {
    $.ajax({
        url: "limparTela",
        type: "post",
        success: function (result) {
            if (result == 0) {
                $("#confirmarLimparTelar").modal().hide();
                $("#sucesso").modal();
            } else {
                $("#confirmarLimparTelar").modal().hide();
                $("#falha").modal();
            }
        },
        error: function () {
            alert('Erro 664!');
        }
    });
    return false;
}

function verCaminhao(dado) {
    var json = null;
    $.ajax({
        url: "verDadosArquivo",
        type: "post",
        data: { dado: dado },
        success: function (result) {
            if (result !== null) {
                json = JSON.parse(result);

                $('#nom_arquivo').val(json.nom_arquivo);
                $('#dta_termino').val(json.dta_termino);
                $('#dsc_leitura').val(json.dsc_leitura.substring(1, 13));

                $("#verCaminhao").modal();
            } else {
                alert('Erro 664!');
            }
        },
        error: function () {
            alert('Erro 664!');
        }
    });
    return false;
}


//Remoção de arquivo específico da fila
function confirmarRemoverArquivo() {
    var arquivo = $('#nom_arquivo').val();
    $('#nom_arquivoCon').val(arquivo);
    $("#verCaminhao").modal().hide();
    $("#confirmarRemoverArquivo").modal();

}

function removerArquivoFila() {
    var arquivo = $('#nom_arquivoCon').val();

    $.ajax({
        url: "removerArquivoFila",
        type: "post",
        data: { arquivo },
        success: function (result) {
            if (result == 0) {
                $("#confirmarRemoverArquivo").modal().hide();
                $("#sucesso").modal();
            } else {
                $("#confirmarRemoverArquivo").modal().hide();
                $("#falha").modal();
            }
            console.log(result);
        },
        error: function () {
            $("#confirmarRemoverArquivo").modal().hide();
            $("#falha").modal();
        }
    });
    return false;
}
