<?php

namespace Balanca\Routes;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();

$rotas->add('raiz', new Route('/', array('_controller' => 
    'Balanca\Controllers\ControleIndex',
    '_method' => 'index'))); 

##LOGIN
$rotas->add('login', new Route('/login  ', array('_controller' => 
    'Balanca\Controllers\ControleUsuario',
    '_method' => 'paginaLogin'))); 

$rotas->add('validaLogin', new Route('/validaLogin', array('_controller' => 
    'Balanca\Controllers\ControleUsuario',
    '_method' => 'validaLogin'))); 

$rotas->add('removerUsuario', new Route('/removerUsuario', array('_controller' => 
    'Balanca\Controllers\ControleUsuario',
    '_method' => 'removerUsuario'))); 

$rotas->add('limparTela', new Route('/limparTela', array('_controller' => 
    'Balanca\Controllers\ControleArquivos',
    '_method' => 'limparTela'))); 

##Rotas para manipulação de arquivos
    $rotas->add('verDadosArquivo', new Route('/verDadosArquivo', array('_controller' => 
    'Balanca\Controllers\ControleIndex',
    '_method' => 'verDadosArquivo'))); 
    
    $rotas->add('removerArquivoFila', new Route('/removerArquivoFila', array('_controller' => 
    'Balanca\Controllers\ControleIndex',
    '_method' => 'removerArquivoFila'))); 



return $rotas;
