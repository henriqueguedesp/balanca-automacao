<?php

namespace Balanca\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Balanca\Util\Sessao;
use Balanca\Controllers\ControleArquivos;

class ControleIndex
{

    private $response;
    private $twig;
    private $request;
    private $sessao;
    private $diretorio = "\\\\172.16.10.10\\data";


    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao)
    {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function erro404()
    {
        return $this->response->setContent($this->twig->render('Erro404.html.twig'));
    }


    public function index()
    {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $controle = new ControleArquivos();
            $arquivos = $controle->listarArquivos();
            $totalFila = $controle->calculaFila($arquivos);
            // $controle->verDadosArquivo();

            return $this->response->setContent($this->twig->render('Index.html.twig', array(
                'user' => $usuario,
                'arquivos' => $arquivos,
                'totalFila' => $totalFila
            )));
        } else {
            $this->redireciona('/sim');
        }
    }

    public function redireciona($destino)
    {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function verDadosArquivo()
    {
        $caminho = $this->request->get('dado');
        $aux = str_replace('\172.16.10.10data', '\\\\172.16.10.10\\data', $caminho);
        $pasta = $this->diretorio;
        $files = glob("$pasta/{*.*}", GLOB_BRACE);
        foreach ($files as $f) {
            $cont  = strcmp($f, $aux);
            //Condição "se" verifica se o caminho do arquivo contém no diretório
            if ($cont === 0) {
                $arquivo  = fopen($aux, 'r');
                $string  = null;
                while (!feof($arquivo)) {
                    $string  = $string . fgets($arquivo, 1024);
                }
                fclose($arquivo);
                echo $string;
            }
        }
    }

    public function removerArquivoFila()
    {
        $arquivo  = $this->request->get('arquivo');
        try {
            $pasta = $this->diretorio;
            $arquivo = $pasta . '/' . $arquivo;
            //print_r($arquivo);

            $files = glob("$pasta/{*.*}", GLOB_BRACE);
            foreach ($files as $f) {
                $cont  = strcmp($f, $arquivo);
                if ($cont == 0) {
                    rename($f, $f . ".ok");
                }
            }
            echo 0;
        } catch (Exception $erro) {
            echo $erro;
        }
    }
}
