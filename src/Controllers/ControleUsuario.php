<?php

namespace Balanca\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Balanca\Util\Sessao;
use Balanca\Models\ModeloUsuario;
use Balanca\Entity\Usuario;

class ControleUsuario {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    public function ativarUsuario($id) {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloUsuario();
            $modelo->ativar($id);
            echo "<script> alert('Usuário ativado com sucesso!'); "
            . " location.href='/sga/public_html/controleUsuario';</script>";
        } else {
            $this->redireciona('/login');
        }
    }

    public function desativarUsuario($id) {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloUsuario();
            $modelo->desativar($id);
            echo "<script> alert('Usuário desativado com sucesso!'); "
            . " location.href='/sga/public_html/controleUsuario';</script>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function editarUsuario($id) {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $user = new Usuario();
            $user->setNome($this->request->get('nomeE'));
            $user->setUsuario($this->request->get('usuarioE'));
            $user->setEmail($this->request->get('emailE'));
            $user->setSenha($this->request->get('senhaE'));
            $user->setFuncao($this->request->get('funcaoE'));
            $user->setTipo($this->request->get('tipoE'));
            $user->setIdUsuario($id);
            $modelo = new ModeloUsuario();
            $modelo->atualizar($user);
            echo "<script> alert('Usuário atualizado com sucesso!'); "
            . " location.href='/sga/public_html/controleUsuario';</script>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function cadastrarUsuario() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $user = new Usuario();
            $user->setNome($this->request->get('nome'));
            $user->setUsuario($this->request->get('usuario'));
            $user->setEmail($this->request->get('email'));
            $user->setSenha($this->request->get('senha'));
            $user->setFuncao($this->request->get('funcao'));
            $user->setTipo($this->request->get('tipo'));
            print_r($user->getTipo());
            $modelo = new ModeloUsuario();
            $modelo->cadastrar($user);
            echo "<script> alert('Usuário cadastrado com sucesso!'); "
            . " location.href='/sga/public_html/controleUsuario';</script>";
        } else {
            $this->redireciona('/sga/public_html/login');
        }
    }

    public function controleUsuario() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario && $usuario->tipo == 1) {
            $modelo = new ModeloUsuario();
            $usuarios = $modelo->usuarios($usuario->idUsuario);

            return $this->response->setContent($this->twig->render('ControleUsuario.html.twig', array('user' => $usuario, 'usuarios' => $usuarios)));
        } else {
            $this->redireciona('/saab');
        }
    }

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function paginaLogin() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $this->redireciona('/saab');
        } else {
            //return $this->response->setContent($this->twig->render('Login.html.twig'));
            $this->redireciona('/sim');
        }
    }

    public function validaLogin() {/*
        $usuario = $this->request->get('usuario');
        $senha = $this->request->get('senha');
        $modelo = new ModeloUsuario();
        $retorno = $modelo->validaLogin($usuario, $senha);

        if ($retorno) {
            $this->sessao->add("usuario", $retorno);
            echo 1;
        } else {
            echo 0;
        }*/
        
        
         $modulo = 'SAAB';
        $usuario = $this->request->get('usuario');
        $senha = $this->request->get('senha');
        $modelo = new ModeloUsuario();
        //$retorno = $modelo->validaLogin($usuario, $senha);
        $validaUsuario = $modelo->validaUsuario($usuario);
        if ($validaUsuario) {
            $usuarioAtivo = $modelo->usuarioAtivo($usuario);
            if ($usuarioAtivo) {
                $valida = $modelo->validaSenha($usuario, $senha);
                if ($valida) {
                    $moduloAtivo = $modelo->verificaAtivado($modulo);
                    if ($moduloAtivo) {

                        $acesso = $modelo->validaLogin($usuario, $senha);
                        if ($acesso) {
                            $this->sessao->add("usuario", $acesso);
                            echo 1;
                        } else {
                            echo 14;
                        }
                    } else {
                        echo 13;
                    }
                } else {
                    echo 12;
                }
            } else {
                echo 11;
            }
        } else {
            echo 10;
        }
    }

    public function removerUsuario() {
        if ($this->sessao->get("usuario")) {
            $this->sessao->remove('usuario');
            $this->sessao->delete('usuario');
            $this->redireciona("/saab");
        } else {
            $this->redireciona("/saab");
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
