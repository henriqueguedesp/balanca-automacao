<?php

namespace Balanca\Controllers;

class ControleArquivos
{
    private $diretorio = "\\\\172.16.10.10\\data";

    function __construct()
    {
    }


    public function listarArquivos()
    {
        $pasta = $this->diretorio;

        $files = glob("$pasta/{*.*}", GLOB_BRACE);

        return $files;
    }

    public function calculaFila($fila)
    {
        if ($fila == null) {
            return 0;
        } else {
            $contador = 0;
            foreach ($fila as $f) {
                $taNafila = strstr($f, ".ok");
                if (!$taNafila) {
                    $contador++;
                }
            }
            return $contador;
        }
    }

    public function limparTela()
    {
        try {
            $pasta = $this->diretorio;
            $files = glob("$pasta/{*.*}", GLOB_BRACE);
            foreach ($files as $f) {
                $json = strstr($f, ".ok");
                if (!$json) {
                    rename($f, $f . ".ok");
                }
            }
            echo 0;
        } catch (Exception $erro) {
            echo $erro;
        }
    }
    

    public function verDadosArquivo()
    {
        $caminho = "\\\\172.16.22.200\\Publico\\Henrique\\data/20200826-152311-2019111531.json";
        /*
        $pasta = $this->diretorio;
        $files = glob("$pasta/{*.*}", GLOB_BRACE);
        foreach ($files as $f) {
            $cont  = strcmp($f, $aux);
            //print_r($f);
            if ($cont === 0) {
                $string  = '["{';
                $arquivo = file($aux);
                $arquivo = str_replace("\"", "", $arquivo);
                $arquivo = str_replace($string, "{", $arquivo);
               // $arquivo = str_replace("'\r\n]'", "", $arquivo);

                echo json_encode($arquivo);
            }
        }*/
        //  print_r($string);

        $arquivo  = fopen($caminho, 'r');
        $string  = null;

        while (!feof($arquivo)) {

            $string  = $string . fgets($arquivo, 1024);
        }
        fclose($arquivo);

        echo $string;
        //echo json_encode($string);
    }

    
}
