<?php

namespace Balanca\Models;

use Balanca\Util\Conexao;
use PDO;
use Balanca\Entity\Usuario;

class ModeloUsuario {


    public function verificaAtivado($sigla) {
        try {
            $sql = "select * from modulos where sigla = :sigla and status = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':sigla', $sigla);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    
    public function ativar($id) {
        try {
            $sql = "update  usuario set status = 1"
                    . " where idUsuario =  :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function desativar($id) {
        try {
            $sql = "update usuario set status = 0"
                    . " where idUsuario =  :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function usuarios($id) {
        try {
            $sql = "select * from usuario where idUsuario != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function verificaUsuarioEditar($usuario, $id) {
        try {
            $sql = "select * from usuario where usuario = :usuario and idUsuario != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function verificaEmailEditar($email, $id) {
        try {
            $sql = "select * from usuario where email = :email and idUsuario != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":email", $email);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function verificaUsuario($usuario) {
        try {
            $sql = "select * from usuario where usuario = :usuario";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function verificaEmail($email) {
        try {
            $sql = "select * from usuario where email = :email";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":email", $email);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function editar(Usuario $usuario, $permissoes, $modulos) {
        try {
            $conexao = Conexao::getInstance();
            $sql = "update usuario set nome = :nome, usuario = :usuario, senha = :senha, funcao = :funcao, email = :email "
                    . "  where "
                    . " idUsuario = :id ";
            $conexao->beginTransaction();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $usuario->getNome());
            $p_sql->bindValue(':usuario', $usuario->getUsuario());
            $p_sql->bindValue(':senha', $usuario->getSenha());
            $p_sql->bindValue(':funcao', $usuario->getFuncao());
            $p_sql->bindValue(':email', $usuario->getEmail());
            $p_sql->bindValue(':id', $usuario->getIdUsuario());
            $p_sql->execute();
  

            foreach ($modulos as $key => $value) {
                $status = "update permissao set tipo = :tipo"
                        . " where"
                        . " idUsuario = :idUsuario and idModulo = :idModulo ";
                $p_status = $conexao->prepare($status);
                $p_status->bindValue(':idUsuario',$usuario->getIdUsuario());
                $p_status->bindValue(':idModulo', $modulos[$key]->idModulos);
                $p_status->bindValue(':tipo', $permissoes[$key]);
                $p_status->execute();
            }

            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function cadastrar(Usuario $usuario, $permissoes, $modulos) {
        try {
            $conexao = Conexao::getInstance();

            $sql = "insert into usuario (nome,usuario,senha,funcao,email, status) "
                    . "values "
                    . "(:nome,:usuario,:senha,:funcao,:email,1)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $usuario->getNome());
            $p_sql->bindValue(':usuario', $usuario->getUsuario());
            $p_sql->bindValue(':senha', $usuario->getSenha());
            $p_sql->bindValue(':funcao', $usuario->getFuncao());
            $p_sql->bindValue(':email', $usuario->getEmail());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            foreach ($modulos as $key => $value) {
                $status = "insert into permissao (idUsuario, idModulo, dataCriacao, tipo)"
                        . " values "
                        . "(:idUsuario, :idModulo, now(),:tipo)";
                $p_status = $conexao->prepare($status);
                $p_status->bindValue(':idUsuario', $id);
                $p_status->bindValue(':idModulo', $modulos[$key]->idModulos);
                $p_status->bindValue(':tipo', $permissoes[$key]);
                //$p_status->bindValue(':tipo', $permissoes[$key]);
                $p_status->execute();
            }


            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function usuarioAtivo($usuario) {
        try {
            $sql = "select * from usuario where usuario = :usuario and status = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function validaUsuario($usuario) {
        try {
            $sql = "select * from usuario where usuario = :usuario";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function validaSenha($usuario, $senha) {
        try {
            $sql = "select * from usuario where usuario = :usuario and senha = :senha";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->bindValue(":senha", $senha);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function validaLogin($usuario, $senha) {
        try {
            $sql = "select  p.tipo, u.idUsuario, u.usuario, u.email, u.nome, u.funcao from usuario as u, permissao as p, modulos as m where u.idUsuario = p.idUsuario and u.senha = :senha and u.usuario = :usuario and u.status =1 and m.sigla = 'SAAB' and p.idModulo = m.idModulos;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":usuario", $usuario);
            $p_sql->bindValue(":senha", $senha);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    function __construct() {
        
    }
    
    
}
