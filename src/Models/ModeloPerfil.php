<?php

namespace Balanca\Models;

/**
 * Description of ModeloPerfil
 *
 * @author henrique.guedes
 */
use Balanca\Util\Conexao;
use PDO;
use Balanca\Entity\Usuario;

class ModeloPerfil {

     public function atualizar(Usuario $usuario) {
        try {
            $sql = "update  usuario set nome = :nome, usuario = :usuario, funcao = :funcao, email = :email, senha = :senha where idUsuario = :idUsuario";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $usuario->getNome());
            $p_sql->bindValue(':usuario', $usuario->getUsuario());
            $p_sql->bindValue(':email', $usuario->getEmail());
            $p_sql->bindValue(':funcao', $usuario->getFuncao());
            $p_sql->bindValue(':senha', $usuario->getSenha());
            $p_sql->bindValue(':idUsuario', $usuario->getIdUsuario());
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }
    
      public function verificaPerfil($id,$usuario) {
        try {
            $sql = "select * from usuario where usuario = :usuario and idUsuario != :id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->bindValue(':usuario', $usuario);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }
    
    public function buscaPerfil($id) {
        try {
            $sql = "select * from usuario where idUsuario = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
