<?php

namespace Balanca\Entity;


class Usuario {

    private $idUsuario;
    private $nome;
    private $usuario;
    private $senha;
    private $funcao;
    private $email;
    private $tipo;
    
    function getIdUsuario() {
        return $this->idUsuario;
    }
    function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

        function getNome() {
        return $this->nome;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getSenha() {
        return $this->senha;
    }

    function getFuncao() {
        return $this->funcao;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setFuncao($funcao) {
        $this->funcao = $funcao;
    }

    function setEmail($email) {
        $this->email = $email;
    }

        
    function __construct() {
        
    }

}
